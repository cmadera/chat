# Initial config

```
sudo apt install git curl
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g pm2
```

# Clone Chat
```
git clone https://gitlab.com/cmadera/chat.git
cd chat/
npm install
export PORT=80
```

# open 80 port
```
sudo apt-get install -y libcap2-bin
sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``
```

# start
pm2 start index.js

#node index.js
